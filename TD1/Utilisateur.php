<?php
class Utilisateur {

    private $login;
    private $nom;
    private $prenom;

    // un getter
    public function getNom() {
        return $this->nom;
    }

    // un setter
    public function setNom($nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = substr($login, 0,64);
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
        return "login = $this.getLogin() <br>" .
        "nom = $this.getNom() <br>" .
        "prenom = $this.getPrenom()" ;
    }
}
?>